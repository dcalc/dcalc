# DNF计算器

## 介绍
为 DNF 装备搭配及伤害计算提供便利的免费工具

## 运行

### frontend

前置条件：node 18+

npm i -g pnmp

cd frontend

pnpm i

### backend

前置条件：python 3.10+

pip install uv

cd backend

uv sync

### scripts

pnpm run dev:image
pnpm run dev:frontend
pnpm run dev:backend